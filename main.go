package main

import (
	"github.com/gin-gonic/gin"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "maxdzh"
	password = "fire"
	dbname   = "maxdzh"
)

func main() {
	start := time.Now()
	log.SetFlags(log.Lshortfile)

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	accounts := make([]Account, 0)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	r, err := zip.OpenReader("tmp/data/data.zip")
	// r, err := zip.OpenReader("tmp/data.zip")
	if err != nil {
		log.Println(err)
	}

	defer r.Close()
	mainData := MainData{}


	// наполняем массив accounts
	for _, f := range r.File {

		rc, err := f.Open()
		if err != nil {
			log.Fatal(err)
		}

		bytes, err := ioutil.ReadAll(rc)
		if err != nil {
			log.Fatal(err)
		}

		rc.Close()

		err = json.Unmarshal(bytes, &mainData)
		if err != nil {
			log.Fatal(err)
		}
		accounts = append(accounts, mainData.Accounts...)
		mainData = MainData{}
	}

	// кладем данные в базу данных
	// начинаем транзакцию
	tx, err := db.Begin()
	defer tx.Rollback()

	stmt, err := tx.Prepare(pq.CopyIn(`account`, `id`, `email`, `fname`, `sname`, `phone`, `sex`, `birth`, `country`, `city`, `joined`, `status`))
	if err != nil {
		log.Fatal(err)
	}

	// аккаунты
	for _, account := range accounts {
		_, err := stmt.Exec(
			account.ID,
			account.Email,
			account.Fname,
			account.Sname,
			account.Phone,
			account.Sex,
			time.Unix(account.Birth, 0),
			account.Country,
			account.City,
			time.Unix(account.Joined, 0),
			account.Status,
		)
		if err != nil {
			log.Fatal(err)
		}

	}

	_, err = stmt.Exec()
	if err != nil {
		log.Fatal(err)
	}

	// лайки
	stmt, err = tx.Prepare(pq.CopyIn(`likes`, `id`, `recipient_id`, `ts`))
	if err != nil {
		log.Fatal(err)
	}
	for _, account := range accounts {

		for _, like := range account.Likes {
			_, err := stmt.Exec(
				account.ID,
				like.ID,
				time.Unix(like.Ts, 0),
			)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	_, err = stmt.Exec()
	if err != nil {
		log.Fatal(err)
	}

	// интересы
	stmt, err = tx.Prepare(pq.CopyIn(`interests`, `id`, `interest`))
	if err != nil {
		log.Fatal(err)
	}
	for _, account := range accounts {

		for _, interest := range account.Interests {
			_, err := stmt.Exec(
				account.ID,
				interest,
			)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	_, err = stmt.Exec()
	if err != nil {
		log.Fatal(err)
	}

	// премиум
	stmt, err = tx.Prepare(pq.CopyIn(`premium`, `id`, `premium_start`, `premium_finish`))
	if err != nil {
		log.Fatal(err)
	}
	for _, account := range accounts {
		emptyPremium := Premium{}
		if account.Premium != emptyPremium {
			_, err := stmt.Exec(
				account.ID,
				time.Unix(account.Premium.Start, 0),
				time.Unix(account.Premium.Finish, 0),
			)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	_, err = stmt.Exec()
	if err != nil {
		log.Fatal(err)
	}

	err = stmt.Close()
	if err != nil {
		log.Fatal(err)
	}

	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Время заполнения базы данных: ", time.Since(start))

	gin.SetMode(gin.DebugMode)
	web := gin.Default()
	web.GET("/ping", pingHanlder)
	web.GET("/accounts/filter/", accountsFilterHandler)
	web.Run(":8089")
}
