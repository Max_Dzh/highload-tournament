APP?=highload-tournament

clean:
	rm -f ${APP}

build: clean
	go build -tags=jsoniter -o ${APP}

run: build
	./${APP}