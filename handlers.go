package main

import (
	"github.com/gin-gonic/gin"
)

func pingHanlder(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}

func accountsFilterHandler(c *gin.Context) {

	allowedParams := map[string]struct{}{
		"sex_eq":             struct{}{},
		"email_domain":       struct{}{},
		"email_lt":           struct{}{},
		"email_gt":           struct{}{},
		"status_eq":          struct{}{},
		"status_neq":         struct{}{},
		"fname_any":          struct{}{},
		"fname_eq":           struct{}{},
		"fname_null":         struct{}{},
		"sname_eq":           struct{}{},
		"sname_starts":       struct{}{},
		"sname_null":         struct{}{},
		"phone_code":         struct{}{},
		"phone_null":         struct{}{},
		"country_eq":         struct{}{},
		"country_null":       struct{}{},
		"city_eq":            struct{}{},
		"city_any":           struct{}{},
		"city_null":          struct{}{},
		"birth_lt":           struct{}{},
		"birth_gt":           struct{}{},
		"birth_year":         struct{}{},
		"interests_contains": struct{}{},
		"interests_any":      struct{}{},
		"likes_contains":     struct{}{},
		"premium_now":        struct{}{},
		"premium_null":       struct{}{},
	}

	// params := c.Request.URL.Query()
	c.JSON(200, gin.H{
		"message": allowedParams,
	})
}
