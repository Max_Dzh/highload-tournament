# FROM alpine
FROM postgres:11.1-alpine
COPY db-init.sql /docker-entrypoint-initdb.d/
WORKDIR app/
COPY highload-tournament .
# потом удалить строки ниже
RUN mkdir tmp
COPY tmp/* ./tmp/
##
COPY run.sh .
EXPOSE 80
CMD ["/bin/sh", "run.sh"]