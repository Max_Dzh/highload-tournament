package main

type MainData struct {
	Accounts []Account `json:"accounts"`
}

type Account struct {
	ID        int      `json:"id"`
	Email     string   `json:"email"`
	Fname     string   `json:"fname,omitempty"`
	Sname     string   `json:"sname,omitempty"`
	Phone     string   `json:"phone,omitempty"`
	Sex       string   `json:"sex"`
	Birth     int64    `json:"birth"`
	Country   string   `json:"country,omitempty"`
	City      string   `json:"city"`
	Joined    int64    `json:"joined"`
	Status    string   `json:"status"`
	Interests []string `json:"interests"`
	Likes     []Like   `json:"likes"`
	Premium   Premium  `json:"premium"`
}

type Like struct {
	ID int   `json:"id"`
	Ts int64 `json:"ts"`
}

type Premium struct {
	Start  int64 `json:"start"`
	Finish int64 `json:"finish"`
}
